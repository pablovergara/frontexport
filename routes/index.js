var express = require('express');
var router = express.Router();
var _ = require('lodash');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const path = require('path');
const fs = require('fs');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

module.exports = router;

//joining path of directory 
const directoryPath = path.join(__dirname, 'json/jsonfinal');

// passsing directoryPath and callback function
fs.readdir(directoryPath, async function (err, files) {
    //handling error
    if (err) return console.log('Unable to scan directory: ' + err);
    // listing all files using forEach
	pathFile = `${__dirname}/json/final.json`
	let dataTotal = await fs.readFileSync(pathFile, 'utf8', async (err, data) => {
		if (err) throw err;
		return data
	})
	dataTotal = JSON.parse(dataTotal)
	await asyncForEach(files, async file => {
		if (file.split('.').pop() === 'json') {
			let pathFile = __dirname + '/json/jsonfinal/' + file
			let data = await fs.readFileSync(pathFile, 'utf8', async (err, data) => {
				if (err) throw err;
				return data
			})
			console.log({file, data})
			dataTotal = _.concat(dataTotal, JSON.parse(data));
		}
	})
	fs.writeFile(__dirname + '/json/final.json', JSON.stringify(dataTotal), 'utf8', async function (err) {
		if (err) return console.log(err);
		console.log("The file was saved!");
	});
});

/**
 * Funcion que genera un foreach asincronico
 * @param array
 * @param callback
 * @returns {Promise<void>}
 */
async function asyncForEach(array, callback) {
	for (let index = 0; index < array.length; index++) {
		await callback(array[index], index, array);
	}
}




